# Nextcloud on Ubuntu 2004

Bash install of Nextcloud 19.0.0 on Ubuntu Server 2004


#################################################################################################

Distributor ID: Ubuntu
Description:    Ubuntu 20.04 LTS
Release:        20.04
Codename:       focal

PHP 7.4.3 (cli) (built: May 26 2020 12:24:22) ( NTS )
Copyright (c) The PHP Group
Zend Engine v3.4.0, Copyright (c) Zend Technologies
    with Zend OPcache v7.4.3, Copyright (c), by Zend Technologies

MariaDB  Server version: 10.5.4-MariaDB-1:10.5.4+maria~focal mariadb.org binary distribution

Nextcloud ver 19.0.0

Instance Build 03/07/20

#################################################################################################

1. Télécharger et Installer Ubuntu Server 2004 sur votre serveur ou votre PC.
   Download and Install Ubuntu Server 2004 on your server or your computer.

2. Dans le fichier "bashInstall.sh", changer les lignes :
   In the file "bashInstall.sh", change the lines :
...(4)gzzegz

3. Dans votre Terminal tapez "nano script.sh". Puis insérer le code de "bashInstall.txt" dedans.
   In your Terminal type "nano script.sh". Then insert the code of "bashInstall.txt" in it.

4. Sortez du fichier et lancez le via "bash script.sh".
   Exit the file and launch it via "bash script.sh".

5. Pour la configuration de Mariadb :
   For the configuration of Mariadb :

...ENTER..
⋅⋅⋅n
⋅⋅⋅n
⋅⋅⋅y
⋅⋅⋅y
⋅⋅⋅y
⋅⋅⋅y

6. Pour la création de l'ulilisateur Nextcloud dans votre base de données, copiez et collez : (Veuillez mettre votre mot de passe à la place de PASSWORD)
   To create the Nextcloud user in your database, copy and paste : (Please put your password in place of PASSWORD)

create database nextclouddb;
create user nextcloud@localhost identified by 'PASSWORD';
grant all privileges on nextclouddb.* to nextcloud@localhost identified by 'PASSWORD';
flush privileges;
exit;

7. Pour le certificat SSL veuillez rentrer vos Informations :
   For the SSL certificate please enter your Informations :

Exemple :
Country Name (2 letter code) [AU]:FR
State or Province Name (full name) [Some-State]:Bourgogne
Locality Name (eg, city) []:Dijon
Organization Name (eg, company) [Internet Widgits Pty Ltd]:PenguinCorp
Organizational Unit Name (eg, section) []:PenguinCorp
Common Name (e.g. server FQDN or YOUR name) []:172.0.0.25
Email Address []:

8. Enable internal debugging in APCu [no] : ENTER

9. Le premier Bash vient de finir veuillez aller sur votre IP pour configurer Nextcloud.
   The first Bash has just finished please go to your IP to configure Nextcloud.

10.(ATTENTION) (WARNING)
   Au moment de la création de ce tuto certains avertissements étaient présents, voici comment régler ces problèmes.
   At the time of the creation of this tutorial certain warnings were present, here's how to fix them.

Errors :
a- PHP getenv("path")
b- PHP limit 512 Mo
c- Pas de cache mémpoire / No memory cache

a- sudo nano /etc/php/7.4/fpm/pool.d/www.conf
   #Décommenter les lignes suivantes en enlevant le ";" du début de chaque ligne
   #Uncomment the following lines by removing the ";" from the beginning of each line
   ;env[HOSTNAME] = $HOSTNAME
   ;env[PATH] = /usr/local/bin:/usr/bin:/bin
   ;env[TMP] = /tmp
   ;env[TMPDIR] = /tmp
   ;env[TEMP] = /tmp

b- sudo nano /usr/share/nginx/nextcloud/.user.ini
   #Ajouter la ligne suivante
   #Add the following line
   memory_limit = 512M

c- sudo nano /usr/share/nginx/nextcloud/config/config.php
   #Ajouter le code en dessous de "'installed' => true,"
   #Add the code below "'installed' => true,"
  'memcache.local' => '\OC\Memcache\APCu',
  'memcache.distributed' => '\OC\Memcache\Memcached',
  'memcached_servers' => array(
     array('localhost', 11211),
    ),

d- sudo reboot

11. Enjoy !
